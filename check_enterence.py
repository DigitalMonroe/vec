import re


def check_login(login):
    """Check for valid login"""
    regex_login = '^[_a-z0-9-]+(\.[_a-z0-9-]+)*$'
    match = re.match(regex_login, login)

    return match

def check_user_address(email):
    """Check for valid user's email address"""
    regex_address = '^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$'
    match = re.match(regex_address, email)
    
    return match
