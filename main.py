#/usr/bin/env python

from resolver import start_resolve
from make_email import make_email
from check_enterence import check_login, check_user_address


fromAddress = 'tmp@tmp.com'

def show_result(successes, fails):
    """Success or unsuccess message"""
    print('Login found at: ')
    for success in successes:
        print('\t' + success)

    print('Failed login at: ')
    for fail in fails:
        print('\t' + fail)

def main():
    if not check_user_address:
        print('Wrong outgoing address')
        return False

    hosts = ['mail.kz', 'gmail.com', 'mail.ru', 'yandex.ru', 'yahoo.com', 'email.ru'] #cheking hosts
    login = input('Enter login: ')

    if not check_login(login):
        print('Wrong login. Login must contain only latin characters, numbers symbols \'.\' and \'_\'')
        return False
    
    emails = make_email(login, hosts)
    success_login = []
    unsuccess_login = []
    
    i = 0
    while i < len(emails):
        if start_resolve(login, hosts[i], fromAddress, emails[i]):
            success_login.append(emails[i])
        else:
            unsuccess_login.append(emails[i])

        i+=1
    
    show_result(success_login, unsuccess_login)

    return True

if __name__ == '__main__':
    if not main():
        exit(1)
    exit(0)
