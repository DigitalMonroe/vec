import smtplib
import dns.resolver

def start_resolve(login, host, fromAddress, email):
    """Trying to check valid email"""
    server = smtplib.SMTP()
    server.set_debuglevel(0)
    records = dns.resolver.query(host, 'MX')
    mxRecords = records[0].exchange
    mxRecords = str(mxRecords)
    
    try:
        server.connect(mxRecords)
        server.helo('testing')
        server.mail(fromAddress)
        code, message = server.rcpt(email)
    except smtplib.SMTPServerDisconnected:
        print('Can\'t get the ' + host + ' server')
        return False
    else:
        server.quit()

        if code == 250:
            return True
        else:
            return False
